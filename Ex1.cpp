#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
using namespace std;

//a.
void I_Love_Threads()
{
	cout << "I love threads" << endl;
}
void Call_I_Love_Threads()
{
	thread t(I_Love_Threads);
	t.join();
}

//b.
void getPrimes(int begin, int end, vector<int>& primes);
void printVector(vector<int> primes);
vector<int> callGetPrimes(int begin, int end);

//d.
void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);

int main()
{
	//Main
	getchar();
	return 0;
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	bool flag;
	for (int i = begin; i <= end; i++)
	{
		flag = false;
		for (int j = 2; j <= (int)sqrt((double)i) && !flag; j++)
			if (i % j == 0)
			{
				flag = true;
			}
		if (!flag)
			primes.push_back(i);
	}
}
void printVector(vector<int> primes)
{
	sort(primes.begin(), primes.end());
	for (vector<int>::iterator it = primes.begin(); it != primes.end(); it++)
		cout << *(it._Ptr) << endl;
}
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> V;
	thread t(getPrimes, begin, end, ref(V));
	clock_t start = clock();
	t.join();
	double duration = (clock() - start) / (double)CLOCKS_PER_SEC;
	cout << duration << " seconds for function" << endl;
	sort(V.begin(), V.end());
	return V;
}

void writePrimesToFile(int begin, int end, ofstream& file)
{
	vector<int> primes = callGetPrimes(begin, end);
	if (!file.is_open())
		file.open();
	string string_num;
	for (int i = 0; i < primes.size(); i++)
	{
		string_num = to_string(primes[i]);
		file.write(string_num.c_str(), string_num.length());
	}
	file.close();
}
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream file;
	file.open(filePath);
	int buffer = (end - begin + 1) / N;
	thread* t = nullptr;
	clock_t start = clock();
	for (int i = 0; i < N; i++)
	{
		if (i + 1 == N)
		{
			t = new thread(writePrimesToFile, begin, end, ref(file));
			t->detach();
		}
		else
		{
			t = new thread(writePrimesToFile, begin, begin + buffer, ref(file));
			t->detach();
			begin += buffer + 1;
		}
	}
	double duration = (clock() - start) / (double)CLOCKS_PER_SEC;
	file.close();
	cout << duration << " seconds until finished" << endl;
	delete t;
}